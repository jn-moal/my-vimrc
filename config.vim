filetype off                    " Reset filetype detection first ...
filetype plugin indent on       " ... and enable filetype detection
set ttyfast                     " Indicate fast terminal conn for faster redraw
if !has('nvim')
set ttymouse=xterm2             " Indicate terminal type for mouse codes
set ttyscroll=3                 " Speedup scrolling
endif
set laststatus=2                " Show status line always
set encoding=utf-8              " Set default encoding to UTF-8
set autoread                    " Automatically read changed files
set autoindent                  " Enabile Autoindent
set backspace=indent,eol,start  " Makes backspace key more powerful.
set incsearch                   " Shows the match while typing
set hlsearch                    " Highlight found searches
set noerrorbells                " No beeps
set number                      " Show line numbers
set showcmd                     " Show me what I'm typing
set noswapfile                  " Don't use swapfile
set nobackup                    " Don't create annoying backup files
set splitright                  " Vertical windows should be split to right
set splitbelow                  " Horizontal windows should split to bottom
set autowrite                   " Automatically save before :next, :make etc.
set hidden                      " Buffer should still exist if window is closed
set fileformats=unix,dos,mac    " Prefer Unix over Windows over OS 9 formats
set noshowmatch                 " Do not show matching brackets by flickering
set noshowmode                  " We show the mode with airline or lightline
set ignorecase                  " Search case insensitive...
set smartcase                   " ... but not it begins with upper case
set completeopt=menu,menuone    " Show popup menu, even if there is one entry
set pumheight=10                " Completion window max size
set nocursorcolumn              " Do not highlight column (speeds up highlighting)
set nocursorline                " Do not highlight cursor (speeds up highlighting)
set lazyredraw                  " Wait to redraw
set shiftwidth=2								" Space for indentation operation (>> or <<)
set tabstop=2										" Number of space that a tab counts for
set expandtab										" Convert tabulation to space
set relativenumber							" Display line number relative to cursor position
set foldmethod=indent           " Setting fold method to indent (default is manual)
set foldlevel=99                " Defines a high fold level so that the file is not folded when opened.
set signcolumn=no               " Always display sign column
set termguicolors               " Enable tru colors support
set cursorline                  " Enable cursor line highlight

" Set hoziontal cursor
set guicursor=n-v-c-sm-i-ci-ve-r-cr-o:hor22

" Enable to copy to clipboard for operations like yank, delete, change and put
" http://stackoverflow.com/questions/20186975/vim-mac-how-to-copy-to-clipboard-without-pbcopy
if has('unnamedplus')
  set clipboard^=unnamed
  set clipboard^=unnamedplus
endif

" This enables us to undo files even if you exit Vim.
if has('persistent_undo')
  set undofile
  set undodir=~/.config/vim/tmp/undo//
endif

" Python specific configuration
let g:python3_host_prog = '/usr/bin/python3.7'

if exists('g:GuiLoaded')
  GuiFont Hack:h9
endif

" Color scheme
let ayucolor="dark"
colorscheme ayu

" LightLine
let g:lightline = {
  \ 'active': {
  \   'right': [ [ 'lineinfo' ],
  \              [ 'percent' ],
  \              [ 'gitbranch', 'fileformat', 'fileencoding', 'filetype' ] ],
  \   'left': [ [ 'mode', 'paste' ],
  \             [ 'readonly', 'filename', 'modified', 'folder' ] ]
  \ },
  \ 'component_function': {
  \   'gitbranch': 'LightLineGitInfo',
  \   'filename': 'LightLineFileName',
  \   'folder': 'LightLineFolderName'
  \ },
  \ }

function! LightLineFileName()
  return expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
endfunction

function! LightLineGitInfo()
  return fugitive#head()
endfunction

function! LightLineFolderName()
  return expand('%:p:h')
endfunction

" terraform plugin configuration

" Syntastic Config
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" (Optional)Remove Info(Preview) window
set completeopt-=preview

" (Optional)Hide Info(Preview) window after completions
autocmd CursorMovedI * if pumvisible() == 0|pclose|endif
autocmd InsertLeave * if pumvisible() == 0|pclose|endif

" (Optional) Enable terraform plan to be include in filter
let g:syntastic_terraform_tffilter_plan = 1

" (Optional) Default: 0, enable(1)/disable(0) plugin's keymapping
let g:terraform_completion_keys = 1

" (Optional) Default: 1, enable(1)/disable(0) terraform module registry completion
let g:terraform_registry_module_completion = 0
