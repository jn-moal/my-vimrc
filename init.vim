source $HOME/.config/nvim/plugins.vim
source $HOME/.config/nvim/config.vim
source $HOME/.config/nvim/mappings.vim
source $HOME/.config/nvim/go.vim
source $HOME/.config/nvim/lsp.vim
source $HOME/.config/nvim/deoplete.vim
