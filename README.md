# My Neovim configuration
If you don't know about [Neovim](https://github.com/neovim/neovim), I suggest you try it out.

This is my Neovim configurations. Feel free to pick up anything that might be usefull for you.  
This project is still, and will always be, a work in progress.

# Plugin Manager
I'm using [dein](https://github.com/Shougo/dein.vim) as a plugin manager.

# Plugins

| Plugin name                                                                               | Short description                                         |
|-------------------------------------------------------------------------------------------|-----------------------------------------------------------|
| [vimproc](https://github.com/Shougo/vimproc.vim)                                          | Asynchronous execution library for vim.                   |
| [vim-rooter](https://github.com/airblade/vim-rooter)                                      | Changes the working directory to the project root         |
| [deoplete](https://github.com/Shougo/deoplete.nvim)                                       | Asynchronous completion framework for Neovim.             |
| [deoplete-go](https://github.com/zchee/deoplete-go)                                       | Asynchronous gocode completion for deoplete.              |
| [vim-go](https://github.com/fatih/vim-go)                                                 | Go language supports for vim.                             |
| [fzf](https://github.com/junegunn/fzf.vim)                                                | Fuzzy file finder                                         |
| [indentLine](https://github.com/Yggdroot/indentLine)                                      | Display thin vertical lines to represent indention level. |
| [ayu-theme](https://github.com/ayu-theme/ayu-vim)                                         | ayu.vim color schemes.                                    |
| [LanguageClient-neovim](https://github.com/autozimu/LanguageClient-neovim)                | LSP support for neovim.                                   |
| [RedHat YAML language server](https://github.com/redhat-developer/yaml-language-server)   | Language server for the yaml file format                  |
| [markdown-preview](https://github.com/iamcco/markdown-preview.nvim)                       | Markdown preview for Vim.                                 |
| [vim-fugitive](https://github.com/tpope/vim-fugitive)                                     | The best git wrapper of all time.                         |
| [fugitive-gitlab](https://github.com/shumphrey/fugitive-gitlab.vim)                       | Open gitlab URLs.                                         |
| [vim-lightline](https://github.com/vim-airline/vim-airline)                               | Status line for Vim.                                      |
| [vim-surround](https://github.com/tpope/vim-surround)                                     | Easily delete/change/add surroundings                     |
| [NERDTree](https://github.com/scrooloose/nerdtree)                                        | File system explorer for VIM                              |
| [NERDTree git](https://github.com/Xuyuanp/nerdtree-git-plugin)                            | Git extension for NERDTree                                |

