" deoplete.nvim
let g:deoplete#enable_at_startup = 1
let b:deoplete_disable_auto_complete = 1
let g:deoplete_disable_auto_complete = 1
let g:deoplete#sources = {}
let g:deoplete#sources#jedi#show_docstring = 1
let g:deoplete#enable_ignore_case = 1
let g:deoplete#enable_smart_case = 1

call deoplete#custom#option('sources', {
  \ 'yaml': ['LanguageClient'],
  \ 'go': ['vim-go']
  \})


