" yaml LSP
let g:LanguageClient_autoStart = 1
let g:LanguageClient_serverCommands = { 
  \ 'yaml': ['node', '~/.vim/dein/repos/github.com/redhat-developer/yaml-language-server/out/server/src/server.js', '--stdio', '--inspect'],
  \ 'sh': ['bash-language-server', 'start'],
  \ 'dockerfile': ['docker-langserver', 'stdio'],
  \ 'python': ['pyls'],
  \ 'ruby': ['solargraph', 'stdio']
  \ }
let g:LanguageClient_settingsPath = '.vim/lsp-settings.json'

nnoremap <F5> :call LanguageClient_contextMenu()<CR>
nnoremap <leader>K :call LanguageClient#textDocument_hover()<CR>
nnoremap <leader>d :call LanguageClient#textDocument_definition()<CR>
nnoremap <F2> :call LanguageClient#textDocument_rename()<CR>
