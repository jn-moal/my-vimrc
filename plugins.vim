if &compatible
  set nocompatible
endif

set runtimepath+=~/.vim/dein/repos/github.com/Shougo/dein.vim " path to dein.vim

if dein#load_state('~/.vim/dein/repos/github.com/Shougo/dein.vim')
  call dein#begin(expand('~/.vim/dein')) " plugins' root path

  call dein#add('Shougo/dein.vim')
  call dein#add('Shougo/vimproc.vim', {
      \ 'build': {
      \     'windows': 'tools\\update-dll-mingw',
      \     'cygwin': 'make -f make_cygwin.mak',
      \     'mac': 'make -f make_mac.mak',
      \     'linux': 'make',
      \     'unix': 'gmake',
      \    },
      \ })
  " vim-rooter
  call dein#add('airblade/vim-rooter')
  " auto-completion plugins
  call dein#add('Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' })

  " Go programming language plugins
  " auto-complete with gocode in deoplete
  call dein#add('zchee/deoplete-go', {'build': 'make', 'on_ft': ['go']})
  " Awesome go development plugins (build, run, test, godoc...)
  call dein#add('fatih/vim-go', {'on_ft': ['go']})
  
  " Trying fzf instead of CtrlP
  call dein#add('junegunn/fzf', { 'build': './install --all' })
  call dein#add('junegunn/fzf.vim')

  " Indent line
  call dein#add('Yggdroot/indentLine')
  " Color scheme
  call dein#add('ayu-theme/ayu-vim')
  
  " LSP client
  call dein#add('autozimu/LanguageClient-neovim', {'rev': 'next', 'build': './install.sh', 'on_ft': ['yaml', 'sh', 'ruby']})
  " YAML LSP
  call dein#add('redhat-developer/yaml-language-server', {'build': 'npm install && npm run compile', 'on_ft': ['yaml']})

  call dein#add('iamcco/markdown-preview.nvim', {'build': 'cd app & yarn install', 'on_ft': ['markdown', 'pandoc.markdown', 'rmd']})
  call dein#add('tpope/vim-fugitive')
  call dein#add('shumphrey/fugitive-gitlab.vim')
  call dein#add('itchyny/lightline.vim')
  call dein#add('tpope/vim-surround')

  " VIM File explorer
  call dein#add('Shougo/defx.nvim')

  " NERDTree plugin
  call dein#add('scrooloose/nerdtree')
  call dein#add('Xuyuanp/nerdtree-git-plugin')
  
  " Ctrl-P plugin
  call dein#add('kien/ctrlp.vim')

  call dein#add('hashivim/vim-terraform', { 'on_ft': ['terraform'] })
  call dein#add('vim-syntastic/syntastic', { 'on_ft': ['terraform'] })
  call dein#add('juliosueiras/vim-terraform-completion', { 'on_ft': ['terraform'] })

  call dein#end()
  call dein#save_state()
endif

